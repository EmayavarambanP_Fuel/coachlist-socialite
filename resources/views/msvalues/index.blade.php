@extends('layouts.app')
@section('content')
        <div class="navbar">
          <a class="brand-logo">Coachlist</a>
          <ul id="nav-menu" class="right">
            @if(Auth::check())
              <li><a>{{ Auth::user()->name }}</a></li>
              <li><a href="/users/logout">Logout</a></li>
             @endif
            
          </ul>
        </div>
       
        <div class="cl-container-2">
          @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

             @if($errors->has())
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        @endif
            <form action="/save" method="POST">
                {{ csrf_field() }}
                <div class="input-group">
                    <label class="cl-label">MsValue</label>
                    <input name="MSValues" type="text" class="cl-input" placeholder="Enter your value here..."/>
                </div>
                <input type="submit" class="cl-cta-btn" value="Save"/>
            </form>
        </div>
   @endsection