<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/auth/login', function () {
    return view('home');
});

Route::get('/msvalues', 'MsvaluesController@index')->middleware('auth');
Route::post('/save', 'MsvaluesController@store');

//Social Login

Route::get('/login/facebook','Auth\AuthController@redirectToProvider');
Route::get('/login/facebook/callback','Auth\AuthController@handleProviderCallback');
Route::get('users/logout', 'Auth\AuthController@getLogout');
