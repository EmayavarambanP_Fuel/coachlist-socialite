<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Msvalue;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MsvaluesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('msvalues.index');  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'msvalue' => 'required|max:255',
    //     ]);
    // }

    public function create(Request $request)
    {   
        

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
         'required' => 'The field is required.',
         'regex' => 'The field will not allow special characters.',
         'max' => 'The maximum limit of the field is 50 characters.'
        ];

       $rules = ['MSValues' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|max:50'];

       $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('/msvalues')
                        ->withErrors($validator)
                        ->withInput();
        }
         $msv = new Msvalue($request->all());
         $msv->save();
         return redirect()->back()->with('message', 'The MSValue has been inserted successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
