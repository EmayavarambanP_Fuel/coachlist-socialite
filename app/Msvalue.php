<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Msvalue extends Model
{
    protected $table = 'MSValues';

    protected $fillable = ['MSValues'];   
}
